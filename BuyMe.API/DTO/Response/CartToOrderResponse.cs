﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuyMe.API.DTO.Response
{
    public class CartToOrderResponse
    {
        public int ProductId { get; set; }
        public int NewMRP { get; set; }
        public int NewDiscount { get; set; }
        public bool InStock { get; set; }
    }
}
