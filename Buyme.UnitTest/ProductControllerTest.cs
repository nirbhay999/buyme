using Buyme.UnitTest.Mocks;
using BuyMe.API.Controllers;
using BuyMe.API.DTO.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using Xunit;
using Moq;
using BuyMe.BL.Interface;
using BuyMe.BL;
using System.Threading.Tasks;
using System.Collections.Generic;
using BuyMe.API.DTO.Response;
using BuyMe.DL.Entities;

namespace Buyme.UnitTest
{
    public class ProductControllerTest
    {



        [Fact]  // signifies the test runner , this is a test method and it has to be run
        public void GetProduct_WhenCalled_ReturnsOkWithProductList()
        {
            // AAA
            // Arrange Act Assert

            // Arrange

            var productSevice = new Mock<IProductService>();
            var list = new List<Product>();
            list.Add(new Product { Id = 1, Name = "Mobile Phone" });
            productSevice.Setup(c => c.GetProducts()).Returns(list);
            var logFactory = LoggerFactory.Create(builder => builder.AddConsole());
            var logger = logFactory.CreateLogger<ProductsController>();
            var productController = new ProductsController(productSevice.Object, logger);

            // Act
            var response = productController.GetProducts();
            var result = response as OkObjectResult;

            // Assert
            Assert.Equal(StatusCodes.Status200OK, result.StatusCode); // Status code must be 200
            
            var body=result.Value as Response<List<ProductResponse>>;  // Type should be wrapped in response dto

            Assert.Equal(list.Count, body.Data.Count);  // item number should match
            for (int i = 0; i < body.Data.Count; i++)
            {
                Assert.Equal(list[i].Id, body.Data[i].Id); // their id should match
            }
        }
    }

       
}
