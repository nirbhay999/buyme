﻿using BuyMe.BL;
using BuyMe.BL.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Buyme.UnitTest.Mocks
{
    public class CartServiceMock : ICartService
    {
        public List<ProductBL> GetItemsFromCart(string email)
        {
            var list = new List<ProductBL>();
            var item1 = new ProductBL()
            {
                Id = 1,
                Name = "test1",
                DiscountPercentage = 10,
                MRPAmount = 200,
                InStock = true
            };
            var item2 = new ProductBL()
            {
                Id = 2,
                Name = "test2",
                DiscountPercentage = 5,
                MRPAmount = 980,
                InStock = true
            };
            list.Add(item1);
            list.Add(item2);
            return list;
        }
    }
}
