﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuyMe.BL
{
    public class RegisterUserBL
    {
     
        public string FirstName { get; set; }
      
        public string LastName { get; set; }
        
        public string PhoneNumber { get; set; }
       
        public string EmailId { get; set; }
        
        public string Password { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}
