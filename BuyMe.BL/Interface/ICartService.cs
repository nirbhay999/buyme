﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuyMe.BL.Interface
{
    public interface ICartService
    {
        public List<ProductBL> GetItemsFromCart(string email);
    }
}
