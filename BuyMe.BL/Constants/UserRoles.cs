﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuyMe.BL.Constants
{
    public static class UserRoles
    {
        public static readonly string Admin = "Admin";
        public static readonly string CustomerSupportLevel1 = "CustomerSupportL1";
    }
}
